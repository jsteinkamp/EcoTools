#' calculate monthly PET, AET, WC, WD based on Thornthwaite
#'
#' @param run the ModelRun object, if desired (see 'as', below).
#' @param T monthly temperature (degree Celcius ) ModelObject, data.table or data.frame
#' @param P monthly precipitation in mm. Needed for everything without PET.
#' @param variable character variable any of 'PET' (potential evapotranspiration), 'AET' (actual evapotranspiration), 'WC' (water content), 'WD' (water deficit).
#' @param as desired output type 'data.table', 'data.frame', anything else will try to return a ModelObject, which need 'run' to be defined.
#' @return the requested variable in the requested ('as') container.
#' @export
#' @import data.table
#' @importFrom methods new
#' @author Joerg Steinkamp \email{joerg.steinkamp@@senckenberg.de}
#' @references Bonan, G. (2002): Ecological Climatology. Chapter 5 (Hydrological Cycle), Cambridge University Press, 690p. \url{http://www.cgd.ucar.edu/tss/aboutus/staff/bonan/ecoclim/index-2002.htm}
thornthwaite <- function(run=NULL, T=NULL, P=NULL, variable="PET", as="data.table") {
  AET.Jan=Annual=P.Jan=PET.Jan=WC.Dec=WC.Jan=WD.Jan=a=Lon=Lat=NULL
  dom <- c(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
  names(dom) <- month.abb
  mid.month <- cumsum(dom) - dom/2

  ## make sure we do not overwrite the original data.table
  if (DGVMTools::is.ModelObject(T)) {
    if (T@is.spatially.averaged)
      stop("Operation not possible for spatially averaged ModelObject!")

    is.temporally.averaged <- T@is.temporally.averaged
    spatial.extent  <- T@spatial.extent
    temporal.extent <- T@temporal.extent
    T <- copy(T@data)
  } else if (is.data.table(T)) {
    T <- copy(T)
  }

  ## check the column names
  if (!all(c("Lat", month.abb)%in%colnames(T)))
    stop(paste("Any of '", paste(c("Lat", month.abb), collapse="', "), "' missing in column names.", sep=""))

  ## calculate the daylength and convert to data.frame
  L <- as.data.frame(t(daylength(T$Lat, mid.month)))
  colnames(L) <- month.abb

  if (is.temporally.averaged) {
    key.names <- c("Lon", "Lat")
  } else {
    key.names <- c("Lon", "Lat", "Year")
  }

  if (is.data.table(T)) {
    L <- cbind(T[, key.names, with=FALSE], L)
  } else {
    L <- as.data.table(cbind(T[, key.names ], L))
  }
  setkeyv(L, key.names)

  ## replace temperatures below zero
  T[, (month.abb):=ifelse(.SD<0, 0, .SD), by=key.names]

  ## calculate some new columns
  T[, I:=rowSums((.SD/5)^1.514), .SDcols=month.abb]
  T[, a:=6.75e-7 * I^3 - 7.71e-5 * I^2 + 1.79e-2 * I + 0.49, by=key.names]

  DT <- L[T]

  formula.str <- NULL
  for (m in month.abb) {
    formula.str <- rbind(formula.str, paste(m, "=16*(", m, "/12)*(dom['", m, "']/30)*(i.", m, "*10/I)^a", sep=""))
  }
  formula.str <- paste(formula.str, collapse=", ")
  PET <- eval(parse(text=paste("DT[, list(", formula.str, "), by=key.names]", sep="")))

  if (toupper(variable)=="PET") {
    if (as == "data.table") {
      return(PET)
    } else if (as == "data.frame") {
      return(as.data.frame(PET))
    } else {
      quant <- new("Quantity",
                   id="pet",
                   name="potential evapotranspiration (Thonthwaite)",
                   type="",
                   units="mm")
      return(new('ModelObject',
                 id="pet",
                 data=PET,
                 quant = quant,
                 spatial.extent = spatial.extent,
                 temporal.extent = temporal.extent,
                 is.site = FALSE,
                 is.temporally.averaged = is.temporally.averaged,
                 is.spatially.averaged = FALSE,
                 run = as(run, "ModelRunInfo")))
    }
  }

  if (DGVMTools::is.ModelObject(P)) {
    P <- copy(P@data)
  } else if (is.data.table(P)) {
    P <- copy(P)
  } else if (is.null(P)) {
    stop("No P (precipitation) data given!")
  }

  DT <- PET[P]

  formula.str <- NULL
  for (m in month.abb) {
    formula.str <- rbind(formula.str, paste(m, "=ifelse(",m,"-i.",m, "<0, 0, ",m,"-i.",m, ")", sep=""))
  }
  formula.str <- paste(formula.str, collapse=", ")
  WD <- eval(parse(text=paste("DT[, list(", formula.str, "), by=key.names]", sep="")))
  WD[ ,Annual:=rowSums(.SD), .SDcols = month.abb]

  if (toupper(variable)=="WD") {
    if (as == "data.table") {
      return(WD)
    } else if (as == "data.frame") {
      return(as.data.frame(WD))
    } else {
      quant <- new("Quantity",
                   id="wd",
                   name="water deficit (Thonthwaite)",
                   type="",
                   units="mm")
      return(new('ModelObject',
                 id="wd",
                 data=WD,
                 quant = quant,
                 spatial.extent = spatial.extent,
                 temporal.extent = temporal.extent,
                 is.site = FALSE,
                 is.temporally.averaged = is.temporally.averaged,
                 is.spatially.averaged = FALSE,
                 run = as(run, "ModelRunInfo")))
    }
  }

  WC.init <- 75.0
  WC.max  <- 150.0
  WC <- data.table(array(WC.init, c(nrow(PET),12)))
  colnames(WC) <- month.abb
  WC <- cbind(PET[, key.names, with=FALSE], WC)
  setkeyv(WC, key.names)

  AET <- data.table(array(0.0, c(nrow(PET),12)))
  colnames(AET) <- month.abb
  AET <- cbind(PET[, key.names, with=FALSE], AET)
  setkeyv(WC, key.names)

  DT <- PET[P]
  setnames(DT, month.abb, paste("PET", month.abb, sep="."))
  setnames(DT, paste("i", month.abb, sep="."), paste("P", month.abb, sep="."))
  DT = WD[DT]
  setnames(DT, month.abb, paste("WD", month.abb, sep="."))
  DT = WC[DT]
  setnames(DT, month.abb, paste("WC", month.abb, sep="."))
  DT = AET[DT]
  setnames(DT, month.abb, paste("AET", month.abb, sep="."))

  DT[, AET.Jan:=ifelse(PET.Jan <= P.Jan, PET.Jan, P.Jan + WD.Jan * WC.Dec/WC.max), by=key.names]
  DT[, WC.Jan:=ifelse(WC.Dec + P.Jan - AET.Jan > WC.max, WC.max, WC.Dec + P.Jan - AET.Jan), by=key.names]
  niterations <- 2
  for (i in 1:(niterations*12)) {
    pm <- month.abb[((i-1)%%12)+1]
    m <- month.abb[(i%%12)+1]
    formula.str <- paste("DT[, AET.", m, ":=ifelse(PET.",m," <= P.", m, ", PET.", m, ", P.", m, " + WD.", m, "*WC.",pm,"/WC.max), by=key.names]",sep="")
    eval(parse(text=formula.str))

    formula.str <- paste("DT[, WC.", m, ":=ifelse(WC.",pm," + P.", m, " - AET.",m," > WC.max, WC.max, WC.", pm, " + P.", m, " - AET.", m, "), by=key.names]", sep="")
    eval(parse(text=formula.str))
  }

  if (toupper(variable) == "WC") {
    WC <- DT[, c(key.names, paste("WC", month.abb, sep=".")), with=FALSE]
    setnames(WC,  paste("WC", month.abb, sep="."), month.abb)
    setkeyv(WC, key.names)
    if (as == "data.table") {
      return(WC)
    } else if (as == "data.frame") {
      return(as.data.frame(WC))
    } else {
      quant <- new("Quantity",
                   id="wc",
                   name="water content (Thonthwaite)",
                   type="",
                   units="mm")
      return(new('ModelObject',
                 id="wc",
                 data=WC,
                 quant = quant,
                 spatial.extent = spatial.extent,
                 temporal.extent = temporal.extent,
                 is.site = FALSE,
                 is.temporally.averaged = is.temporally.averaged,
                 is.spatially.averaged = FALSE,
                 run = as(run, "ModelRunInfo")))
    }
  }

  if (toupper(variable) == "AET") {
    AET <- DT[, c(key.names, paste("AET", month.abb, sep=".")), with=FALSE]
    setnames(AET,  paste("AET", month.abb, sep="."), month.abb)
    setkeyv(AET, key.names)
    if (as == "data.table") {
      return(AET)
    } else if (as == "data.frame") {
      return(as.data.frame(AET))
    } else {
      quant <- new("Quantity",
                   id="AET",
                   name="actual evapotranspiration (Thonthwaite)",
                   type="",
                   units="mm")
      return(new('ModelObject',
                 id="aet",
                 data=AET,
                 quant = quant,
                 spatial.extent = spatial.extent,
                 temporal.extent = temporal.extent,
                 is.site = FALSE,
                 is.temporally.averaged = is.temporally.averaged,
                 is.spatially.averaged = FALSE,
                 run = as(run, "ModelRunInfo")))
    }
  }
}
